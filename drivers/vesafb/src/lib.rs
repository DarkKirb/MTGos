#![no_std]

extern crate fbtty;
extern crate multiboot2;
extern crate hal_base;
use hal_base::TTY;

pub fn get_tty(boot_info: &multiboot2::BootInformation) -> Option<fbtty::FBTTY> {
    match boot_info.framebuffer_tag() {
        None => None,
        Some(fbtag) => {
            match fbtag.buffer_type {
                multiboot2::FramebufferType::RGB { red: _, green: _, blue: _ } => (),
                _ => {return None;}
            };
            let mut tty = fbtty::FBTTY {
                x: 0,
                y: 0,
                width: (fbtag.width / 8) as u16,
                height: (fbtag.height / 8) as u16,
                stride: fbtag.pitch,
                rot: fbtty::Rotation::Rot0,
                pos: (fbtag.address as usize) as *mut u8
            };
            tty.clear();
            Some(tty)
        }
    }
}
