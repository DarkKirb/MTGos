#![no_std]

extern crate n3dsi2c;

pub fn enableTopLCD() {
    let mut i2c = n3dsi2c::I2C::new();
    i2c.selectDev(3);
    i2c.selectReg(34);
    i2c.write(0b100010);
}

pub fn enableBottomLCD() {
    let mut i2c = n3dsi2c::I2C::new();
    i2c.selectDev(3);
    i2c.selectReg(34);
    i2c.write(0b1010);
}
