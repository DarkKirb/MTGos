CC = clang
AS = clang
LD = ld.lld
CARGO = xargo

TARGET ?= amd64
DEVICE ?= pc

include targets.mk

FLAGS = -ffunction-sections -fdata-sections -Wall -Wextra -Werror -Wno-error=unused -Wno-error=unused-variable -flto $(ARCH_FLAGS)
CFLAGS = $(FLAGS) -std=c11 -g
ASFLAGS = $(ARCH_FLAGS) -g
LDFLAGS = -Tlinker/$(TARGET)-$(DEVICE).ld -g -O3
CARGOFLAGS = --target=$(TARGET)-$(DEVICE)

BOOT_SRCS = $(shell find boot/$(TARGET)/$(DEVICE) -name '*.s') $(shell find boot/$(TARGET)/$(DEVICE) -name '*.c')
BOOT_OBJS = $(addsuffix .o,$(basename $(BOOT_SRCS)))
HAL_SRCS = $(shell find hal/$(TARGET)/$(DEVICE) -name '*.s') $(shell find boot/$(TARGET)/$(DEVICE) -name '*.c')
HAL_OBJS = $(addsuffix .o,$(basename $(HAL_SRCS)))
KERNEL_SRCS = $(shell find . -name '*.rs')

all:
	rm -rf out
	mkdir out
	$(MAKE) TARGET=amd64 DEVICE=pc clean
	$(MAKE) TARGET=amd64 DEVICE=pc build
	mv mtgos.iso out/amd64-pc.iso
	$(MAKE) TARGET=x86 DEVICE=pc clean
	$(MAKE) TARGET=x86 DEVICE=pc build
	mv mtgos.iso out/x86-pc.iso
	$(MAKE) TARGET=arm DEVICE=raspi2 clean
	$(MAKE) TARGET=arm DEVICE=raspi2 build
	mv mtgos out/kernel7.img
	$(MAKE) TARGET=arm DEVICE=3ds9 clean
	$(MAKE) TARGET=arm DEVICE=3ds9 build
	mv mtgos out/kernel9
	$(MAKE) TARGET=arm DEVICE=3ds11 clean
	$(MAKE) TARGET=arm DEVICE=3ds11 build
	mv mtgos out/kernel11
	cd tools/firmlink; \
	cargo run --release ../../out/mtgos.firm ../../out/kernel9 ../../out/kernel11

ifeq ($(DEVICE),pc)
build: mtgos.iso
mtgos.iso: mtgos iso/boot/grub/grub.cfg
	cp mtgos iso/
	grub-mkrescue -o $@ iso/

iso/boot/grub/grub.cfg:
	mkdir -p iso/boot/grub
	echo "set timeout=0" > $@
	echo "set default=0" >> $@
	echo "menuentry \"MTGos\" {" >> $@
	echo "    multiboot2 /mtgos" >> $@
	echo "    boot" >> $@
	echo "}" >> $@
else
build: mtgos
endif

mtgos: $(BOOT_OBJS) hal.a kernel.a
	$(LD) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.o: %.s
	$(AS) $(ASFLAGS) -c -o $@ $<

hal.a: $(HAL_OBJS)
	ar rcs $@ $^

kernel/Cargo.toml: kernel/template.toml
	cp kernel/template.toml kernel/Cargo.toml
	echo "[dependencies.hal]" >> kernel/Cargo.toml
	echo "path = \"../hal/$(TARGET)/$(DEVICE)\"" >> kernel/Cargo.toml

kernel.a: kernel/Cargo.toml $(KERNEL_SRCS)
	cd kernel; \
	RUST_TARGET_PATH=$(shell pwd)/targets $(CARGO) build $(CARGOFLAGS)
	mv kernel/target/$(TARGET)-$(DEVICE)/debug/libkernel.a kernel.a

clean:
	rm -rf boot.a hal.a kernel.a
	rm -rf $(BOOT_OBJS)
	rm -rf $(HAL_OBJS)
	rm -rf kernel/target
	rm -rf kernel/Cargo.toml
	rm -rf mtgos.iso
	rm -rf iso

.PHONY: clean
