#![no_std]
#![feature(core_intrinsics)]
extern crate n3dsmcu;
extern crate hal_base;
extern crate fbtty;

struct PDC {
    pixelClock: u32, // 0x00
    HBlankTimer: u32, // 0x04
    unk0x08: u32,
    unk0x0C: u32,
    verticalPixelInterpolation: u32, // 0x10
    horizontalDataOffset: u32, // 0x14
    unk0x18: u32,
    unk0x1C: u32,
    horizontalPixelDataOffset: u32, // 0x20
    unk0x24: u32,
    VBlankTimer: u32, // 0x28
    unk0x2C: u32,
    VTotal: u32, // 0x30
    VDisp: u32, // 0x34
    verticalDataOffset: u32, // 0x38
    unk0x3C: u32,
    unk0x40: u32,
    unk0x44: u32,
    unk0x48: u32,
    overscanFilterColor: u32, // 0x4C
    HPos: u32, // 0x50
    VPos: u32, // 0x54
    unk0x58: u32,
    framebufferSize: u32, // 0x5C
    totalWidth: u32, // 0x60
    totalHeight: u32, // 0x64
    fba1: *mut u8, // 0x68
    fba2: *mut u8, // 0x6C
    framebufferFormat: u32, // 0x70
    unk0x74: u32,
    framebufferSelect: u32, // 0x78
    unk0x7C: u32,
    colorLUTIndex: u32, // 0x80
    colorLUTElem: u32, // 0x84
    unk0x88: u32,
    unk0x8C: u32,
    stride: u32, // 0x90
    fbb1: *mut u8, // 0x94
    fbb2: *mut u8, // 0x98
    unk0x9C: u32
}

unsafe fn write(pos: usize, data: u32) {
    *(pos as *mut u32) = data;
}

pub fn init_gpu() {
    n3dsmcu::enableTopLCD();
    n3dsmcu::enableBottomLCD();
    let ptr = 0 as *mut u32;
    unsafe {
        write(0x10141200, 0x1007F); // Enable Backlights and GPU IO Region
        write(0x10202014, 1); // Unknown
        *(0x1020200C as *mut u32) &= 0xFFFEFFFE; // Unknown
        write(0x10202240, 0x3F); // Top screen brightness
        write(0x10202A40, 0x3F); // Bottom screen brightness
        write(0x10202244, 0x1023F);
        write(0x10202A44, 0x1023F);
        //PDC0 framebuffer setup
        let pdc0 = 0x10400400 as *mut PDC;
        (*pdc0).pixelClock = 0x1c2;
        (*pdc0).HBlankTimer = 0xd1;
        (*pdc0).unk0x08 = 0x1c1;
        (*pdc0).unk0x0C = 0x1c1;
        (*pdc0).verticalPixelInterpolation = 0;
        (*pdc0).horizontalDataOffset = 0xcf;
        (*pdc0).unk0x18 = 0xd1;
        (*pdc0).unk0x1C = 0x1c501c1;
        (*pdc0).horizontalDataOffset = 0x10000;
        (*pdc0).unk0x24 = 0x19d;
        (*pdc0).VBlankTimer = 2;
        (*pdc0).unk0x2C = 0x192;
        (*pdc0).VTotal = 0x192;
        (*pdc0).VDisp = 0x192;
        (*pdc0).verticalDataOffset = 1;
        (*pdc0).unk0x3C = 2;
        (*pdc0).unk0x40 = 0x01960192;
        (*pdc0).unk0x44 = 0;
        (*pdc0).unk0x48 = 0;
        (*pdc0).framebufferSize = 0x00f00190;
        (*pdc0).totalWidth = 0x01c100d1;
        (*pdc0).totalHeight = 0x01920002;
        (*pdc0).fba1 = 0x18000000 as *mut u8;
        (*pdc0).fba2 = 0x18046500 as *mut u8;
        (*pdc0).fbb1 = 0x1808ca00 as *mut u8;
        (*pdc0).fbb2 = 0x180d2f00 as *mut u8;
        (*pdc0).framebufferFormat = 0x80341;
        (*pdc0).unk0x74 = 0x00010501;
        (*pdc0).framebufferSelect = 0;
        (*pdc0).stride = 0x000002D0;
        (*pdc0).unk0x9C = 0x00000000;
        (*pdc0).colorLUTIndex = 0;
        for i in 0..256 {
            core::intrinsics::volatile_store(&mut ((*pdc0).colorLUTElem) as *mut u32, 0x10101 * i as u32);
        }
        let pdc1 = 0x10400500 as *mut PDC;
        (*pdc1).pixelClock = 0x1c2;
        (*pdc1).HBlankTimer = 0xd1;
        (*pdc1).unk0x08 = 0x1c1;
        (*pdc1).unk0x0C = 0x1c1;
        (*pdc1).verticalPixelInterpolation = 0;
        (*pdc1).horizontalDataOffset = 0xcf;
        (*pdc1).unk0x18 = 0xd1;
        (*pdc1).unk0x1C = 0x1c501c1;
        (*pdc1).horizontalDataOffset = 0x10000;
        (*pdc1).unk0x24 = 0x19d;
        (*pdc1).VBlankTimer = 2;
        (*pdc1).unk0x2C = 0x192;
        (*pdc1).VTotal = 0x192;
        (*pdc1).VDisp = 0x192;
        (*pdc1).verticalDataOffset = 1;
        (*pdc1).unk0x3C = 2;
        (*pdc1).unk0x40 = 0x01960192;
        (*pdc1).unk0x44 = 0;
        (*pdc1).unk0x48 = 0;
        (*pdc1).framebufferSize = 0x00f00140;
        (*pdc1).totalWidth = 0x01c100d1;
        (*pdc1).totalHeight = 0x01920052;
        (*pdc1).fba1 = 0x18119400 as *mut u8;
        (*pdc1).fbb1 = 0x18151800 as *mut u8;
        (*pdc1).framebufferFormat = 0x80301;
        (*pdc1).unk0x74 = 0x00010501;
        (*pdc1).framebufferSelect = 0;
        (*pdc1).stride = 0x000002D0;
        (*pdc1).unk0x9C = 0x00000000;
        (*pdc1).colorLUTIndex = 0;
        for i in 0..256 {
            core::intrinsics::volatile_store(&mut ((*pdc1).colorLUTElem) as *mut u32, 0x10101 * i as u32);
        }

    }
}

pub fn get_tty() -> fbtty::FBTTY {
    fbtty::FBTTY {
        x: 0,
        y: 0,
        width: 50,
        height: 30,
        stride: 0x2D0,
        rot: fbtty::Rotation::Rot90,
        pos: 0x18000000 as *mut u8 
    }
}
