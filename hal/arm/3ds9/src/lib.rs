#![no_std]

extern crate hal_base;
extern crate fbtty;

static mut fbtty: fbtty::FBTTY = fbtty::FBTTY {
    x: 0,
    y: 0,
    width: 40,
    height: 30,
    stride: 0x2D0,
    rot: fbtty::Rotation::Rot90,
    pos: 0x18119400 as *mut u8
};

pub fn tty() -> &'static mut hal_base::TTY {
    unsafe {
        &mut fbtty
    }
}

#[no_mangle]
pub extern fn platform_init() {
}

#[no_mangle]
pub unsafe extern fn  __sync_val_compare_and_swap_1(ptr: *mut u8, expected: u8, desired: u8) -> u8 {
    let cur = *ptr;
    if cur == expected {
        *ptr = desired;
    }
    cur
}

#[no_mangle]
pub unsafe extern fn  __sync_val_compare_and_swap_2(ptr: *mut u16, expected: u16, desired: u16) -> u16 {
    let cur = *ptr;
    if cur == expected {
        *ptr = desired;
    }
    cur
}

#[no_mangle]
pub unsafe extern fn  __sync_val_compare_and_swap_4(ptr: *mut u32, expected: u32, desired: u32) -> u32 {
    let cur = *ptr;
    if cur == expected {
        *ptr = desired;
    }
    cur
}

