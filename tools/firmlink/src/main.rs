extern crate byteorder;
extern crate elf;
extern crate sha2;
use byteorder::{LittleEndian, WriteBytesExt};

use std::env;
use std::path::PathBuf;
use std::fs::File;
use std::io::prelude::*;
use sha2::{Sha256, Digest};

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 4 {
        panic!("Usage: {} <output FIRM> <arm9 ELF> <arm11 ELF>", args[0]);
    }
    let arm9_path = PathBuf::from(args[2].clone());
    let arm11_path = PathBuf::from(args[3].clone());
    let arm9 = elf::File::open_path(&arm9_path).unwrap();
    let arm11 = elf::File::open_path(&arm11_path).unwrap();
    let arm9text = arm9.get_section(".text").unwrap();
    let arm11text = arm11.get_section(".text").unwrap();
    let mut outf = File::create(args[1].clone())?;
    outf.write_all(b"FIRM")?;  // Magic
    outf.write_u32::<LittleEndian>(0)?;  // Boot Priority
    outf.write_u32::<LittleEndian>(arm11.ehdr.entry as u32)?; // ARM11 entrypoint
    outf.write_u32::<LittleEndian>(arm9.ehdr.entry as u32)?; // ARM9 entrypoint
    outf.write_all(&[0 as u8; 0x30])?; // Unused
    outf.write_u32::<LittleEndian>(0x200)?; // ARM9 section location
    outf.write_u32::<LittleEndian>(arm9text.shdr.addr as u32)?; // Start of arm9 section
    outf.write_u32::<LittleEndian>(arm9text.shdr.size as u32)?; // Size of arm9 section
    outf.write_u32::<LittleEndian>(2)?; // Copy using memcpy
    let mut hasher = Sha256::default();
    hasher.input(&arm9text.data[..]);
    outf.write_all(&hasher.result()[..])?; // sha256 hash of section
    let file_off = match arm9text.data.len() % 0x200 {
        0 => arm9text.data.len() + 0x200,
        x => 0x400 - x + arm9text.data.len()
    };
    outf.write_u32::<LittleEndian>(file_off as u32)?; //ARM11 section location
    outf.write_u32::<LittleEndian>(arm11text.shdr.addr as u32)?; // Start of arm11 section
    outf.write_u32::<LittleEndian>(arm11text.shdr.size as u32)?; // Size of arm11 section
    outf.write_u32::<LittleEndian>(2)?; // Copy using memcpy
    hasher = sha2::Sha256::default();
    hasher.input(&arm11text.data[..]);
    outf.write_all(&hasher.result()[..])?; // sha256 hash of section
    outf.write_all(&[0 as u8; 0x30 * 2][..])?; // Empty sections
    outf.write_all(b"\xB6\x72\x45\x31\xC4\x48\x65\x7A\x2A\x2E\xE3\x06\x45\x7E\x35\x0A\x10\xD5\x44\xB4\x28\x59\xB0\xE5\xB0\xBE\xD2\x75\x34\xCC\xCC\x2A\x4D\x47\xED\xEA\x60\xA7\xDD\x99\x93\x99\x50\xA6\x35\x7B\x1E\x35\xDF\xC7\xFA\xC7\x73\xB7\xE1\x2E\x7C\x14\x81\x23\x4A\xF1\x41\xB3\x1C\xF0\x8E\x9F\x62\x29\x3A\xA6\xBA\xAE\x24\x6C\x15\x09\x5F\x8B\x78\x40\x2A\x68\x4D\x85\x2C\x68\x05\x49\xFA\x5B\x3F\x14\xD9\xE8\x38\xA2\xFB\x9C\x09\xA1\x5A\xBB\x40\xDC\xA2\x5E\x40\xA3\xDD\xC1\xF5\x8E\x79\xCE\xC9\x01\x97\x43\x63\xA9\x46\xE9\x9B\x43\x46\xE8\xA3\x72\xB6\xCD\x55\xA7\x07\xE1\xEA\xB9\xBE\xC0\x20\x0B\x5B\xA0\xB6\x61\x23\x6A\x87\x08\xD7\x04\x51\x7F\x43\xC6\xC3\x8E\xE9\x56\x01\x11\xE1\x40\x5E\x5E\x8E\xD3\x56\xC4\x9C\x4F\xF6\x82\x3D\x12\x19\xAF\xAE\xEB\x3D\xF3\xC3\x6B\x62\xBB\xA8\x8F\xC1\x5B\xA8\x64\x8F\x93\x33\xFD\x9F\xC0\x92\xB8\x14\x6C\x3D\x90\x8F\x73\x15\x5D\x48\xBE\x89\xD7\x26\x12\xE1\x8E\x4A\xA8\xEB\x9B\x7F\xD2\xA5\xF7\x32\x8C\x4E\xCB\xFB\x00\x83\x83\x3C\xBD\x5C\x98\x3A\x25\xCE\xB8\xB9\x41\xCC\x68\xEB\x01\x7C\xE8\x7F\x5D\x79\x3A\xCA\x09\xAC\xF7")?;
    // Perfect signature, courtesy of SciresM, Myriachan, Normmatt, TuxSH and Hedgeberg
    outf.write_all(&arm9text.data[..])?;
    match arm9text.data.len() % 0x200 {
        0 => {},
        x => { outf.write_all(&[0 as u8; 0x200][x..])?; }
    };
    outf.write_all(&arm11text.data[..])?;
    Ok(())
}
