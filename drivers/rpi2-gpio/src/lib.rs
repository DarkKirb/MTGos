#![no_std]

extern crate hal_base;

fn delay(count: usize) {
    for i in 0..count {}
}

pub fn uart_init() {
    let gpio = 0x3F200000 as *mut u32;
    let uart = 0x3F201000 as *mut u32;
    unsafe {
        *uart.offset(0x30) = 0; // disable uart0
        *gpio.offset(0x94) = 0; // disable pull up/down for all gpio pins
        delay(150);
        *gpio.offset(0x98) = (1 << 14) | (1 << 15); // disable pull up/down for pin 14, 15
        delay(150);
        *gpio.offset(0x98) = 0; // make it take effect
        *uart.offset(0x44) = 0x7FF; //Clear pending interrupts
        *uart.offset(0x24) = 1; // set divider integer part
        *uart.offset(0x28) = 40; // set divier fractional part
        *uart.offset(0x2C) = 0b1110000; // Enable fifo & 8 bit data transmission (1 stop bit no parity)
        *uart.offset(0x38) = 0b11111110010; // Mask all interrupts
        *uart.offset(0x30) = 0b1100000001; // enable uart0, receive & transfer part of uart.
    }
}

pub fn uart_putb(c: u8) {
    let uart = 0x3F201000 as *mut u32;
    while unsafe { *uart.offset(0x18) } & 0x20 != 0 {} // wait for uart to be ready to send
    unsafe {
        *uart = c as u32;
    }
}

pub fn uart_getb() -> u8 {
    let uart = 0x3F201000 as *const u32;
    while unsafe { *uart.offset(0x18) } & 0x10 != 0 {} // wait for uart to be ready to recv
    unsafe {
        *uart as u8
    }
}

pub struct SerialTTY {}

impl hal_base::TTY for SerialTTY {
    fn put_char(&mut self, c: char) {
        let mut buf: [u8; 4] = [0; 4];
        c.encode_utf8(&mut buf);
        for b in buf.iter() {
            uart_putb(*b);
        }
    }
    fn get_pos(&self) -> (u16, u16) {
        (0, 0) // size not supported
    }
    fn set_pos(&mut self, pos: (u16, u16)) {}
    fn get_size(&self) -> (u16, u16) {(0, 0)}
    fn clear(&mut self) {
        self.print("\x1B[2J");
    }
}
