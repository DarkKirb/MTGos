#![no_std]

pub trait TTY {
    fn put_char(&mut self, c: char);
    fn print(&mut self, string: &str) {
        for c in string.chars() {
            self.put_char(c);
        }
    }
    fn get_pos(&self) -> (u16, u16);
    fn set_pos(&mut self, pos: (u16, u16));
    fn get_size(&self) -> (u16, u16);
    fn clear(&mut self);
}
