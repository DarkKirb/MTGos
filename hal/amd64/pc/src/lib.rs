#![no_std]

extern crate hal_base;
extern crate cga;
extern crate vesafb;
extern crate fbtty;
extern crate multiboot2;

use cga::CGATTY;

static mut cgatty: CGATTY = CGATTY {
    x: 0,
    y: 0
};

static mut vesatty: Option<fbtty::FBTTY> = None;


pub fn tty() -> &'static mut hal_base::TTY {
    unsafe {
        match vesatty {
            None => &mut cgatty,
            Some(_) => vesatty.as_mut().unwrap()
        }
    }
}

pub fn write_to_fb(data: &[u8], color: u8) {
    let buffer_ptr = 0xB8000 as *mut u8;
    for (i, char_byte) in data.iter().enumerate() {
        unsafe {
            *(buffer_ptr.offset((i * 2) as isize)) = *char_byte;
            *(buffer_ptr.offset((i * 2 + 1) as isize)) = color;
        }
    }
}

static mut bootInfo: Option<multiboot2::BootInformation> = None;

#[no_mangle]
pub extern fn platform_init(magic: u32, pos: u32) {
    match magic {
        0x36d76289 => Some(()),
        _ => None
    }.unwrap();
    unsafe {
        bootInfo = Some(multiboot2::load(pos as usize));
        vesatty = vesafb::get_tty(bootInfo.as_ref().unwrap());
    }
}
