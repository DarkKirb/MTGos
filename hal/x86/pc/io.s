.global _inb
.global _inw
.global _inl
.global _outb
.global _outw
.global _outl

_inb:
    mov %di, %dx
    inb %dx, %al
    ret

_inw:
    mov %di, %dx
    inw %dx, %ax
    ret

_inl:
    mov %di, %dx
    inl %dx, %eax
    ret

_outb:
    mov %si, %ax
    mov %di, %dx
    outb %al, %dx
    ret

_outw:
    mov %si, %ax
    mov %di, %dx
    outw %ax, %dx
    ret

_outl:
    mov %esi, %eax
    mov %di, %dx
    outl %eax, %dx
    ret
