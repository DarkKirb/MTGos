#![no_std]

extern {
    fn _inb(port: u16) -> u8;
    fn _inw(port: u16) -> u16;
    fn _inl(port: u16) -> u32;
    fn _outb(port: u16, data: u8);
    fn _outw(port: u16, data: u16);
    fn _outl(port: u16, data: u32);
}

pub unsafe fn in_u8(port: u16) -> u8 {
    _inb(port)
}

pub unsafe fn in_u16(port: u16) -> u16 {
    _inw(port)
}

pub unsafe fn in_u32(port: u16) -> u32 {
    _inl(port)
}

pub unsafe fn out_u8(port: u16, data: u8) {
    _outb(port, data)
}

pub unsafe fn out_u16(port: u16, data: u16) {
    _outw(port, data)
}

pub unsafe fn out_u32(port: u16, data: u32) {
    _outl(port, data)
}
