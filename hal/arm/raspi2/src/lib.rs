#![no_std]

extern crate hal_base;
extern crate rpi2_gpio;

static mut fbtty: rpi2_gpio::SerialTTY = rpi2_gpio::SerialTTY {};

pub fn tty() -> &'static mut hal_base::TTY {
    unsafe {
        &mut fbtty
    }
}

#[no_mangle]
pub extern fn platform_init() {
    rpi2_gpio::uart_init();
}
