#![no_std]

extern crate io_in_out;
extern crate hal_base;
extern crate iconv;

pub struct CGATTY {
    pub x: u16,
    pub y: u16
}

impl hal_base::TTY for CGATTY {
    fn put_char(&mut self, c: char) {
        match c {
            '\n' => {
                self.x = 0;
                self.y += 1;
                if self.y == 25 {
                    self.clear();
                }
                return;
            },
            '\r' => {
                self.x = 0;
                return;
            },
            '\0' => {
                return;
            }
            _ => ()
        };
        let buffer_ptr = 0xB8000 as *mut u8;
        unsafe {
            *(buffer_ptr.offset((self.y as isize * 80 + self.x as isize) * 2)) = match iconv::unicode_to_cp437(c) {
                None => { return; }
                Some(x) => x
            };
            *(buffer_ptr.offset((self.y as isize * 80 + self.x as isize) * 2 + 1)) = 0x07;
        }
        self.x += 1;
        if self.x == 80 {
            self.y += 1;
            if self.y == 25 {
                self.clear();
            } else {
                self.x = 0;
            }
        }
        self.redraw_cursor();
    }
    fn get_pos(&self) -> (u16, u16) {
        (self.x, self.y)
    }
    fn set_pos(&mut self, pos: (u16, u16)) {
        self.x = pos.0;
        self.y = pos.1;
        self.redraw_cursor();
    }
    fn get_size(&self) -> (u16, u16) {
        (80, 25)
    }
    fn clear(&mut self) {
        self.x = 0;
        self.y = 0;
        let buffer_ptr = 0xB8000 as *mut _;
        unsafe {
            *buffer_ptr = [0 as u8; 4000];
        }
        self.redraw_cursor();
    }
}
impl CGATTY {
    fn redraw_cursor(&self) {
        let off = self.x + self.y * 80;
        unsafe {
            io_in_out::out_u8(0x3D4, 15);
            io_in_out::out_u8(0x3D5, off as u8);
            io_in_out::out_u8(0x3D4, 14);
            io_in_out::out_u8(0x3D5, (off >> 8) as u8);
        }        
    }
}

