#![no_std]

#[repr(packed)]
struct I2CBus {
    pub data: u8,
    pub ctl: u8,
    pub cntex: u16,
    pub scl: u16
}

static mut buses: [*mut I2CBus; 3] = [
    0x10161000 as *mut I2CBus,
    0x10144000 as *mut I2CBus,
    0x10148000 as *mut I2CBus
];

static busids: [usize; 18] = [1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 2, 1, 3];
static busaddrs: [usize; 18] = [0x4a, 0x7a, 0x78, 0x4a, 0x78, 0x2C, 0x2E, 0x40, 0x44,
                                0xA6, 0xD0, 0xD2, 0xA4, 0x9A, 0xA0, 0xEE, 0x40, 0x54];

pub struct I2C {
    currDev: usize,
    currReg: usize,
    busID: usize
}
impl I2C {
    pub fn new() -> I2C {
        I2C { 
            currDev: 0,
            currReg: 0,
            busID: 0
        }
    }
    pub fn waitBusy(&self) {
        while unsafe { (*buses[self.busID]).ctl } & 0x80 != 0 {}
    }
    pub fn getResult(&self) -> bool {
        self.waitBusy();
        (unsafe { (*buses[self.busID]).ctl } & 0x10) != 0
    }
    pub fn stop(&self) {
        unsafe {
            (*buses[self.busID]).ctl = 0xC0;
        }
        self.waitBusy();
        unsafe {
            (*buses[self.busID]).ctl = 0xC0;
        }
    }
    pub fn selectDev(&mut self, dev: usize) -> bool {
        self.currDev = dev;
        self.busID = busids[dev];
        self.waitBusy();
        unsafe {
            (*buses[self.busID]).data = busaddrs[dev] as u8;
            (*buses[self.busID]).ctl = 0xC2;
        }
        self.getResult()
    }
    pub fn selectReg(&mut self, reg: usize) -> bool {
        if self.currDev == 3 && reg == 5 { // Cowardly refuse to hardbrick 3DS
            return false;
        }
        self.currReg = reg;
        self.waitBusy();
        unsafe {
            (*buses[self.busID]).data = reg as u8;
            (*buses[self.busID]).ctl = 0xC0;
        }
        self.getResult()
    }
    pub fn write(&mut self, data: u8) -> bool {
        for _ in 0..10 {
            self.waitBusy();
            unsafe {
                (*buses[self.busID]).data = data;
                (*buses[self.busID]).ctl = 0xC1;
            }
            if self.getResult() {
                return true;
            }
            self.stop();
            let dev = self.currDev;
            let reg = self.currReg;
            self.selectDev(dev);
            self.selectReg(reg);
        }
        false
    }
    pub fn read(&self) -> Option<u8> {
        self.waitBusy();
        unsafe {
            (*buses[self.busID]).ctl = 0xE1;
        }
        self.waitBusy();
        let data = unsafe { (*buses[self.busID]).data };
        unsafe {
            (*buses[self.busID]).ctl |= 0x10;
        }
        let out = match self.getResult() {
            true => Some(data),
            false => None
        };
        self.stop();
        out
    }
}
