ifeq ($(TARGET),amd64)
ARCH_FLAGS := -target x86_64-elf -mno-red-zone
CPU_FLAGS := -target x86_64-elf
else ifeq ($(TARGET),x86)
ARCH_FLAGS := -target i386-elf
CPU_FLAGS := -target i386-elf
else ifeq ($(TARGET),arm)
ARCH_FLAGS := -target arm-none-eabi
CPU_FLAGS := -target arm-none-eabi
ifeq ($(DEVICE),3ds11)
ARCH_FLAGS += -march=armv6k -mcpu=mpcore -mfloat-abi=hard
CPU_FLAGS += -march=armv6k -mcpu=mpcore -mfloat-abi=hard
else ifeq ($(DEVICE),3ds9)
ARCH_FLAGS += -march=armv5te -mcpu=arm946e-s
CPU_FLAGS += -march=armv5te -mcpu=arm946e-s
else ifeq ($(DEVICE),raspi2)
ARCH_FLAGS += -march=armv7a -mtune=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=hard
CPU_FLAGS += -march=armv7a -mtune=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=hard
else
$(error "Unknown device for target arm $(DEVICE)")
endif
else
$(error "Unknown target $(TARGET)")
endif
