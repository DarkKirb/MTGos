#![no_std]

extern crate hal_base;
extern crate picafb;
extern crate fbtty;

static mut fbtty: Option<fbtty::FBTTY> = None;

pub fn tty() -> &'static mut hal_base::TTY {
    unsafe {
        fbtty.as_mut().unwrap()
    }
}

#[no_mangle]
pub extern fn platform_init() {
    picafb::init_gpu();
    unsafe {
        fbtty = Some(picafb::get_tty());
    }
}
