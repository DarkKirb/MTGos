
#![feature(lang_items)]
#![feature(panic_implementation)]
#![no_std]

extern crate rlibc;
extern crate hal;

use core::panic::PanicInfo;

#[no_mangle]
pub extern fn rust_main() {
    let tty = hal::tty();
    tty.clear();
    tty.print("Hello, World! This is an example\ntext that spans multiple lines!");
}

#[lang = "eh_personality"]
#[no_mangle]
pub extern fn eh_personality() {}

#[panic_handler]
#[no_mangle]
pub extern fn panic_fmt(_info: &PanicInfo) -> ! {
    loop{}
}
