#![no_std]

extern crate pdfont;
extern crate hal_base;
extern crate rlibc;

pub enum Rotation {
    Rot0,
    Rot90,
    Rot180,
    Rot270
}

fn rotate_coords(pos: (isize, isize), max: (isize, isize), to: &Rotation) -> (isize, isize) {
    match to {
        Rotation::Rot0 => pos,
        Rotation::Rot90 => (max.1 - pos.1, pos.0),
        Rotation::Rot180 => (max.0 - pos.0, max.1 - pos.1),
        Rotation::Rot270 => (pos.0, max.1 - pos.1)
    }
}

pub struct FBTTY {
    pub x: u16,
    pub y: u16,
    pub width: u16,
    pub height: u16,
    pub stride: u32,
    pub rot: Rotation,
    pub pos: *mut u8
}

impl hal_base::TTY for FBTTY {
    fn put_char(&mut self, c: char) {
        match c {
            '\n' => {
                self.x = 0;
                self.y += 1;
                if self.y == 25 {
                    self.clear();
                }
                return;
            },
            '\r' => {
                self.x = 0;
                return;
            },
            '\0' => {
                return;
            }
            _ => ()
        };
        for (row, byte) in pdfont::get_glyph(c).iter().enumerate() {
            for (column, bit_no) in (0..8).enumerate() {
                let (x, y) = rotate_coords((self.x as isize * 8 + column as isize, self.y as isize * 8 + row as isize), (self.width as isize * 8, self.height as isize * 8), &self.rot);
                unsafe {
                    *(self.pos.offset(y * self.stride as isize + x * 3) as *mut _) = match *byte & (1 << bit_no) {
                        0 => [0 as u8; 3],
                        _ => [0xFF as u8; 3]
                    }
                }
            }
        }
        self.x += 1;
        if self.x == self.width {
            self.y += 1;
            if self.y == self.height {
                self.clear();
            } else {
                self.x = 0;
            }
        }
    }
    fn get_pos(&self) -> (u16, u16) {
        (self.x, self.y)
    }
    fn set_pos(&mut self, pos: (u16, u16)) {
        self.x = pos.0;
        self.y = pos.1;
    }
    fn get_size(&self) -> (u16, u16) {
        (self.width, self.height)
    }
    fn clear(&mut self) {
        self.x = 0;
        self.y = 0;
        unsafe {
            rlibc::memset(self.pos, 0, self.height as usize * self.width as usize * 8 * 8 * 3);
        }
    }
}
